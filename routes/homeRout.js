const express = require("express");
const homeController = require("../controllers/homeController.js");
const homeRout = express.Router();
 
homeRout.get("/", homeController.index);
 
module.exports = homeRout;