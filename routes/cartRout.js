const express = require("express");
const cartController = require("../controllers/cartController.js");
const cartRout = express.Router();
const auth = require ("../auth.js");


cartRout.post("/create", auth.verifyToken, cartController.createCart);
cartRout.get("/:id", auth.verifyToken, cartController.getCartById);
cartRout.put("/add", auth.verifyToken, cartController.addProductToCart);

module.exports = cartRout;
