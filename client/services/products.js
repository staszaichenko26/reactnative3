
import { Alert } from "react-native";

const URL = 'http://192.168.1.112:3000'

export async function getAllProducts(token) {
    const result = await fetch(`${URL}/product/get/all`,{
        method:'GET',
        headers:{
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + token
        }
    })
    
    if (result.status == 200) {
    const json = await result.json()
        return json.products 
    } 
    else if (result.status == 401)
    {
        Alert.alert ('Ошибка','Товары отсутствуют');
        return false;
    }
    else {
        Alert.alert ('Ошибка','Что то пошло не так');
        return false;
    }
}

export async function addProductToCart (name, token) {
    const result = await fetch(`${URL}/cart/add`,{
        method:'PUT',
        body: JSON.stringify(name),
        headers:{
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + token
        }
    })
    if (result.status == 200) {
        Alert.alert ('Отлично','Товар добавлен в корзину');
    } 
    else {
        Alert.alert ('Ошибка','Что то пошло не так');
        return false;
    }
}