import { Button, StyleSheet, Text, View,TextInput, TouchableHighlight, ImageBackground, Alert} from 'react-native';
import React, {useState,useContext} from 'react';
import { logIn } from '../services/login';
import Context from '../utils/context';
export default function HomeScreen({ navigation }) {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState("");
  const context = useContext(Context)
  async function login (){
    const result = await logIn ({email,password});
    if (result) {
      const name = result.name
      const products = result.products
      const surname = result.surname
      const token = result.token
      const cartId = result.cart._id
      const notification = false;
      context.handleShowUser({ name, surname, products, token, cartId, notification })
      navigation.navigate('Main')
    }
  }
  
  const img = {uri:"https://png.pngtree.com/thumb_back/fw800/background/20190703/pngtree-vector-white-background-with-abstract-geometric-pattern-of-3d-tr-image_282961.jpg"}
    return (
      <View style={styles.container}>
        <ImageBackground source={img} resizeMode="cover" style={styles.image}>
        <Text style={{ ...styles.titleText }}>Введите логин</Text>
        <TextInput style={styles.textInput}  onChangeText={text => setEmail(text)} value={email} placeholder="email" />
        <Text style={{ ...styles.titleText }}>Введите пароль</Text>
        <TextInput style={styles.textInput} onChangeText={text => setPassword(text)} value={password} placeholder="пароль"  />
        <Button  onPress={() => login()} title="LogIn"  />
        <TouchableHighlight
          style={styles.touch}
          onPress={() => navigation.navigate('Registration')}>
          <Text style={styles.ref}>Регистрация</Text>
        </TouchableHighlight>
        </ImageBackground>
      </View>
    );
  }

  const styles = StyleSheet.create({
    touch: {
      marginTop: 50
    },
    ref: {
      textDecorationLine: "underline",
      color:"black"
    },
    image: {
      width:"100%",
      flex: 1,
      alignItems:"center"
    },
    titleText: {
      color: "black",
      fontSize: 20
    },
    textInput: {
      color:"blue",
      height: 30,
      width: 150,
      borderColor: 'darkgrey',
      borderWidth: 3,
      marginBottom:30,
      paddingLeft: 5
      
    },
    container: {
      flex: 1,
      backgroundColor: '#d1d1d1',
      alignItems: "center",
      justifyContent:"center"
    },
  });