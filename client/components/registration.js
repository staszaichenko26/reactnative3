import {StyleSheet, StatusBar, Text, View, Button, TextInput, Switch, CheckBox, ImageBackground } from 'react-native';
import React, { useContext, useEffect, useState } from 'react';
import Context from '../utils/context';
import { registration } from '../services/auth';

export default function RegScreen({ navigation, route }) {

  async function Registration() {
    const result = await registration({ name, surname, email, password });
    if (result) {
      const token = result.token
      const cartId = result.cart._id
      context.handleShowUser({ name,token, surname, password, isEnabled, isSelected, cartId })
      navigation.navigate('Main')
    }
  }

  const img = { uri: "https://png.pngtree.com/thumb_back/fw800/background/20190703/pngtree-vector-white-background-with-abstract-geometric-pattern-of-3d-tr-image_282961.jpg" }
  const { info } = route.params
  const [name, setName] = useState('');
  const [surname, setSurname] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState("");
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);
  const [isSelected, setSelection] = useState(false);
  const context = useContext(Context)
  useEffect(() => {
    if (info) {
      setName(context.userState.name),
        setSurname(context.userState.surname),
        setPassword(context.userState.password),
        setEmail(context.userState.email),
        setIsEnabled(context.userState.isEnabled),
        setSelection(context.userState.isSelected)
    }
  }, [info])

  return (
    <View style={styles.container}>
      <ImageBackground source={img} resizeMode="cover" style={styles.image}>
        <Text style={{ ...styles.mainTitle }}>Store</Text>
        <StatusBar style="auto" />
        <Text style={{ ...styles.titleText }}>Введите имя</Text>
        <TextInput style={styles.textInput} onChangeText={text => setName(text)} placeholder="имя" value={name} />
        <Text style={{ ...styles.titleText }}>Введите фамилию</Text>
        <TextInput style={styles.textInput} placeholder="фамилия" onChangeText={text => setSurname(text)} value={surname} />
        <Text style={{ ...styles.titleText }}>Введите пароль</Text>
        <TextInput style={styles.textInput} placeholder="пароль" onChangeText={text => setPassword(text)} value={password} />
        <Text style={{ ...styles.titleText }}>Введите email</Text>
        <TextInput style={styles.textInput} placeholder="email" onChangeText={text => setEmail(text)} value={email} />
        <Text style={{ ...styles.titleText }}>Получать рассылку?</Text>
        <Switch trackColor={{ false: '#d90d0d', true: '#00059c' }} thumbColor={isEnabled ? '#d90d0d' : '#00059c'}
          onValueChange={toggleSwitch} value={isEnabled} />
        <Text style={{ ...styles.titleText }}>Довольны ли вы сервисом?</Text>
        <CheckBox value={isSelected} onValueChange={setSelection} style={styles.checkbox} />
        <Button onPress={() => Registration()} title="Зарегистрироваться" />
      </ImageBackground>
    </View>
  );
}


const styles = StyleSheet.create({

  image: {
    width: "100%",
    flex: 1,
    alignItems: "center"
  },
  mainTitle: {
    fontSize: 50,
    fontWeight: "bold",
    color: "lightblue"
  },
  titleText: {
    color: "black",
    fontSize: 20
  },
  textInput: {
    color: "black",
    height: 30,
    width: 150,
    borderColor: 'darkgrey',
    borderWidth: 3,
    paddingLeft: 5
  },
  container: {
    flex: 1,
    backgroundColor: '#d1d1d1',
    alignItems: "center",
    justifyContent: "space-evenly"
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  openButton: {
    backgroundColor: 'red',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    color: "white",
    textAlign: 'center',
  },
});