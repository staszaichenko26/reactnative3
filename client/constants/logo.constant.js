import React,{Image} from "react";

export default function LogoTitle() {
    return (
        <Image
            style={{ width: 50, height: 50 }}
            source={require('../assets/magic.png')}
        />
    );
}