import React  from "react";
import { Image,Button} from 'react-native';
import ScreenOne from '../components/drawScreen1';
import ScreenTwo from '../components/drawScreen2';
import { createDrawerNavigator } from '@react-navigation/drawer';
import BottomTabNavigator from "./TabNavigator";
const Drawer = createDrawerNavigator();

function LogoTitle() {
    return (
        <Image
            style={{ width: 50, height: 50 }}
            source={require('../assets/profile.png')}
        />
    );
} 

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator >
        <Drawer.Screen name="MainDraw" component={BottomTabNavigator} options={({ navigation, route })  => ({
                headerTitle: (props) => <LogoTitle {...props} />, headerRight:()=> (
                    <Button onPress={() => navigation.navigate('Home')} title="Log Out"/>
                )})}  />
      <Drawer.Screen name="ScreenOne" component={ScreenOne} options={({ navigation, route }) => ({
                headerTitle: (props) => <LogoTitle {...props} />, headerRight:()=> (
                    <Button onPress={() => navigation.navigate('Home')} title="Log Out"/>)
            })}  />
      <Drawer.Screen name="ScreenTwo" component={ScreenTwo} options={({ navigation, route }) => ({
                headerTitle: (props) => <LogoTitle {...props} />,  headerRight:()=> (
                    <Button onPress={() => navigation.navigate('Home')} title="Log Out"/>)
            })} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;