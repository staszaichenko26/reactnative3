import React, {useEffect, useContext} from "react";
import ProductScreen from '../components/products';
import ProfileScreen from '../components/main';
import CartScreen from '../components/cart';
import OrderScreen from '../components/orders';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { orderGet } from '../services/orders';
import Context from "../utils/context";
import { getUserProducts } from "../services/userProducts";

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {

  const context = useContext(Context)
  useEffect(() => {
    (async () => {
      const orders = await orderGet(context.userState.token);
      const ordLength = orders.length
      context.userState.ordLength = ordLength
      const productsArr = await getUserProducts(context.userState.cartId, context.userState.token);
      const prodLength = productsArr.length
      context.userState.prodLength = prodLength
    })()
  },)

  return (
    <Tab.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ color, size }) => {
        let iconName;
        if (route.name === 'Profile') {
          iconName = 'body';
        } else if (route.name === 'Products') {
          iconName ='clipboard';
        }
        else if (route.name === 'Cart') {
          iconName ='cart';
        }
        else if (route.name === 'Orders') {
          iconName ="receipt";
        }
        return <Ionicons name={iconName} size={size} color={color} />;
      },
      tabBarActiveTintColor: 'red',
      tabBarInactiveTintColor: 'gray',
    })}>
      <Tab.Screen name="Profile" component={ProfileScreen}  options={{ headerShown: false }} />
      <Tab.Screen name="Products" component={ProductScreen} options={{ headerShown: false }}/>
      <Tab.Screen name="Cart" initialParams={{param:true}} component={CartScreen} options={{ headerShown: false, tabBarBadge: context.userState.prodLength }} />
      <Tab.Screen name="Orders" initialParams={{param:true}} component = {OrderScreen} options={{ headerShown: false, tabBarBadge: context.userState.ordLength }}  />
    </Tab.Navigator>
  );
};

export default BottomTabNavigator;